import errno
import os

from itertools import ifilter
from shutil import rmtree
from urllib import quote

from gphoto.backup import PhotosDownloader


class FSPhotosDownloader(PhotosDownloader):
    def __init__(self, config, credentials):
        self.target_dir = config.dir
        super(FSPhotosDownloader, self).__init__(config, credentials)

    @classmethod
    def add_subparser(cls, subparsers):
        parser = subparsers.add_parser(
            'fs', help='Write photos to the local file system')
        parser.add_argument('dir', help='destination directory', default='.')
        return parser

    def download_albums(self):
        keep = set()

        for album in self._list_albums():
            self.log('Processing album_entry %s' % album)
            self.download_album(album)
            keep.add(album.slug)

        # cleanup deleted albums:
        for dn in ifilter(lambda dn_: dn_ not in keep,
                          os.listdir(self.target_dir)):
            dn = os.path.join(self.target_dir, dn)

            self.log('removing deleted album %s' % dn)
            try:
                if os.path.isdir(dn):
                    rmtree(dn, ignore_errors=True)
                else:
                    os.unlink(dn)
            except OSError as e:
                if e.errno != errno.ENOENT:
                    self.log('failed to delete album %s: %s' % (dn, str(e)))

    def download_album(self, album):
        path = os.path.join(self.target_dir, album.slug)

        if not os.path.exists(path):
            os.mkdir(path)

        keep = set()
        for photo in self._list_photos(album):
            self.download_photo(photo)
            keep.update(photo.slug + ext for ext in
                        ('', '.etag.' + quote(photo.etag, safe='"')))

        # cleanup deleted files:
        for fn in ifilter(lambda _fn: _fn not in keep, os.listdir(path)):
            fn = os.path.join(path, fn)
            self.log('removing deleted file %s' % fn)
            try:
                os.unlink(fn)
            except OSError as e:
                if e.errno != errno.ENOENT:
                    self.log('failed to delete %s: %s' % (fn, str(e)))

    def write_file(self, photo, stream):
        dir_ = os.path.join(self.target_dir, photo.album.slug)
        etagfile = (os.path.join(dir_, photo.slug) +
                    '.etag.' + quote(photo.etag, safe='"'))

        # remove old etag(s):
        map(os.unlink,
            (os.path.join(dir_, fn) for fn in
             ifilter(lambda fn_: fn_.startswith(photo.slug + '.etag.'),
                     os.listdir(dir_))))

        # write file:
        with open(os.path.join(self.target_dir, photo.fullslug), 'wb') as f:
            for chunk in stream:
                f.write(chunk)

        # create new etag:
        with open(etagfile, 'w'):
            pass

    def is_stale(self, photo):
        etagfile = (os.path.join(self.target_dir, photo.fullslug) +
                    '.etag.' + quote(photo.etag, safe='"'))

        return not os.path.exists(etagfile)
