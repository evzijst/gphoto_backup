import argparse
import os
import posixpath
from itertools import ifilter
from tempfile import TemporaryFile

from threading import current_thread
from urllib import quote
from urlparse import urlsplit

from boto3 import client, resource as boto_resource
from botocore.exceptions import ClientError

from gphoto.backup import PhotosDownloader, chunker

ENVS = ('AWS_ACCESS_KEY_ID', 'AWS_SECRET_ACCESS_KEY')


def s3_url_type(x):
    parts = urlsplit(x)
    if not parts.scheme == 's3':
        raise argparse.ArgumentTypeError('Invalid S3 URL scheme for: %s' % x)

    if not all(v in os.environ for v in ENVS):
        raise argparse.ArgumentTypeError(
            'S3 credentials missing from environment variables: %s' %
            ', '.join(ENVS))

    try:
        # test connection and bucket:
        boto_resource('s3').meta.client.head_bucket(Bucket=parts.netloc)
    except ClientError as e:
        raise argparse.ArgumentTypeError('Connection to S3 failed: %s' % str(e))
    return parts


class S3PhotosDownloader(PhotosDownloader):
    def __init__(self, config, credentials):
        self.buckets = {}
        self.prefix = config.url.path.lstrip('/') + '/'
        super(S3PhotosDownloader, self).__init__(config, credentials)

    def get_bucket(self):
        if current_thread().ident not in self.buckets:
            self.buckets[
                current_thread().ident] = boto_resource('s3').Bucket(
                self.config.url.netloc)
        return self.buckets[current_thread().ident]

    @classmethod
    def add_subparser(cls, subparsers):
        parser = subparsers.add_parser(
            's3', help='Store photos in S3')
        parser.add_argument('url', type=s3_url_type,
                            help='destination on S3 (e.g. s3://evzijst/backup)')
        return parser

    def download_albums(self):
        bucket = self.get_bucket()

        keep = {self.prefix + '/'}
        for album in self._list_albums():
            self.log('Processing album_entry %s' % album)
            self.download_album(album)
            keep.add(posixpath.join(self.prefix, album.slug) + '/')

        # cleanup deleted albums:
        remove = set()
        for result in client('s3').get_paginator('list_objects').paginate(
                Bucket=self.config.url.netloc, Prefix=self.prefix,
                Delimiter='/'):
            for prefix in (cp.get('Prefix') for cp in
                           result.get('CommonPrefixes') if
                           cp.get('Prefix') not in keep):
                self.log('removing deleted album %s' % prefix)
                remove.update(
                    o.key for o in bucket.objects.filter(Prefix=prefix))
        for keys in chunker(remove, 1000):
            bucket.delete_objects(
                Delete={'Objects': [{'Key': k} for k in keys]})

    def download_album(self, album):
        bucket = self.get_bucket()

        # download list of all keys under the album prefix:
        keys = {obj.key for obj in
                bucket.objects.filter(
                    Prefix=posixpath.join(self.prefix, album.slug) + '/')}

        keep = set()
        for photo in self._list_photos(album):
            if posixpath.join(self.prefix,
                              '%s.etag.%s' % (
                                      photo.fullslug,
                                      quote(photo.etag, safe='"'))) in keys:
                self.log('skipping unchanged entry %s' % photo.fullslug)
            else:
                # remove old etag(s):
                p = posixpath.join(self.prefix, photo.slug + '.etag.')
                for chunk in chunker(
                        ifilter(lambda fn_: fn_.startswith(p), keys), 1000):
                    bucket.delete_objects(
                        Delete={'Objects': [{'Key': k} for k in chunk]})

                # download new/updated file:
                self.download_photo(photo)

            keep.update(
                (posixpath.join(self.prefix, photo.fullslug + ext) for ext in
                 ('', '.etag.' + quote(photo.etag, safe='"'))))

        # cleanup deleted files:
        for fns in chunker(ifilter(lambda _fn: _fn not in keep, keys), 1000):
            for fn in fns:
                self.log('removing deleted file %s' % fn)

            bucket.delete_objects(
                Delete={'Objects': [{'Key': fn} for fn in fns]})

    def write_file(self, photo, stream):
        key = posixpath.join(self.prefix, photo.fullslug)
        etagkey = key + '.etag.' + quote(photo.etag, safe='"')

        # write file:
        with TemporaryFile() as f:
            # Boto3 only supports uploads from seekable streams, so download to
            # local file first:
            for chunk in stream:
                f.write(chunk)
            f.seek(0)
            self.get_bucket().put_object(Key=key, Body=f, ACL='private',
                                         ServerSideEncryption='AES256')

        # create new etag:
        self.get_bucket().put_object(Key=etagkey, Body='', ACL='private',
                                     ServerSideEncryption='AES256')

    def is_stale(self, photo):
        return True
