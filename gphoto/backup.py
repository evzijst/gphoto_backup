#!/usr/bin/env python
#
# References:
#
# * https://developers.google.com/gdata/docs/2.0/reference
# * https://developers.google.com/picasa-web/docs/2.0/developers_guide_protocol
# * https://developers.google.com/picasa-web/docs/2.0/reference

import time

from Queue import Empty, Queue
from itertools import ifilter, chain, izip_longest
from threading import RLock, Thread, current_thread
from urllib import urlencode
from urlparse import urlsplit, parse_qs, urlunsplit

from requests_oauthlib import OAuth2Session

MAX_RETRIES = 3


def create_session(credentials):
    session = OAuth2Session(
        client_id=credentials['google_client_id'],
        auto_refresh_url='https://www.googleapis.com/oauth2/v4/token',
        auto_refresh_kwargs={'client_id': credentials['google_client_id'],
                             'client_secret': credentials['google_client_secret']},
        scope=['https://picasaweb.google.com/data/'],
        token={
            'access_token': 'xxx',
            'token_type': 'Bearer',
            'refresh_token': credentials['google_refresh_token'],
            'expires_in': -1},
        token_updater=lambda token: setattr(session, 'token', token))
    session.headers.update({'GData-Version': '3'})
    return session


def link(entry, rel):
    return next(ifilter(
        lambda i: i['rel'] == rel, entry.get('link', [])), {}).get('href')


def chunker(iterable, n):
    """Returns a generator that consumes the specified iterable and returns
    its contents in chunks of n items.

    >>> chunker('ABCDEFG', 3) --> 'ABC' 'DEF' 'G'
    """
    return (filter(None, c) for c in izip_longest(*([iter(iterable)] * n)))


def patch_url_params(url, **params):
    """Inserts the specified query parameters into the url's query string."""
    parts = urlsplit(url)
    params_ = parse_qs(parts.query)

    for k, v in params.iteritems():
        params_.setdefault(k, [])
        params_[k].append(v)

    qs = urlencode(list(
        chain(*([(k, v) for v in l] for k, l in params_.iteritems()))))
    return urlunsplit(
        (parts.scheme, parts.netloc, parts.path, qs, parts.fragment))


class Album(object):
    def __init__(self, album_entry):
        self.album_entry = album_entry

    @property
    def feed_url(self):
        return link(self.album_entry, 'http://schemas.google.com/g/2005#feed')

    @property
    def slug(self):
        return self.album_entry['gphoto$name']['$t']

    @property
    def etag(self):
        return self.album_entry['gd$etag']

    def __str__(self):
        return self.slug


class Photo(object):
    def __init__(self, photo_entry, album):
        self.photo_entry = photo_entry
        self.album = album

    @property
    def download_url(self):
        return (self.photo_entry['gphoto$videodownloadurl']['$t'] if
                'gphoto$videodownloadurl' in self.photo_entry else
                self.photo_entry['content']['src'])

    @property
    def slug(self):
        return self.photo_entry['title']['$t']

    @property
    def etag(self):
        return self.photo_entry['gd$etag']

    @property
    def fullslug(self):
        return '%s/%s' % (self.album.slug, self.slug)

    def __str__(self):
        return self.slug


class DownloadWorker(Thread):
    def __init__(self, downloader):
        self.downloader = downloader
        self.session = create_session(downloader.credentials)
        self._shutdown = False
        super(DownloadWorker, self).__init__(target=self.run,
                                             name=self.__class__.__name__)

    def shutdown(self):
        """Signal the worker to exit after completing its current job."""
        self._shutdown = True

    def download_file(self, photo):
        try:
            resp = self.downloader.request(self.session, photo.download_url,
                                           stream=True)
            self.downloader.log('downloading file %s' % photo.fullslug)

            def counter(stream):
                for c in stream:
                    counter.size += len(c)
                    yield c
            counter.size = 0

            start = time.time()
            self.downloader.write_file(photo, counter(resp.iter_content(64 * 1024)))
            duration = time.time() - start
            self.downloader.log(
                '%s completed (%s bytes in %.2fs -- %.2fkb/s)' % (
                    photo.fullslug, counter.size, duration,
                    (counter.size / float(duration)) / 1024))
        except Exception as e:
            self.downloader.log('failed to download %s %s -- skipping' %
                                (photo.fullslug, str(e)))

    def run(self):
        self.downloader.log('Started worker')
        try:
            while not self._shutdown:
                try:
                    photo = self.downloader.q.get(timeout=1)
                except Empty:
                    continue
                else:
                    if self.downloader.is_stale(photo):
                        self.download_file(photo)
                    else:
                        self.downloader.log('skipping unchanged entry %s' %
                                            photo.fullslug)
        finally:
            self.downloader.log('exiting')


class PhotosDownloader(object):
    logging_mutex = RLock()

    def __init__(self, config, credentials):
        self.config = config
        self.credentials = credentials
        self.q = Queue(100)
        self._session = None

    @classmethod
    def add_subparser(cls, subparsers):
        pass

    def log(self, msg):
        with self.logging_mutex:
            print '%d %s' % (current_thread().ident, msg)

    def request(self, session, url, **kwargs):
        e = None
        for i in xrange(1, MAX_RETRIES + 1):
            try:
                r = session.get(url, **kwargs)
                r.raise_for_status()
                return r
            except IOError as _e:
                e = _e
                self.log('retrying request (%d/%d) for %s %s' % (
                    i, MAX_RETRIES, url, str(e)))
            self.log('giving up on %s' % url)
        raise e

    def run(self):
        workers = []
        try:
            for i in xrange(self.config.workers):
                w = DownloadWorker(self)
                w.daemon = True
                workers.append(w)
                w.start()

            self._session = create_session(self.credentials)
            self.download_albums()

            # wait for the queue to drain
            self.log('Waiting for the job queue to drain...')
            while not self.q.empty():
                time.sleep(1)
        finally:
            for w in workers:
                w.shutdown()
            self.log('Waiting for workers to exit...')
            for w in workers:
                w.join()
            self.log('All workers finished')

    def _list_albums(self, user='default'):
        url = patch_url_params(
            'https://picasaweb.google.com/data/feed/api/user/%s' % user,
            **{'kind': 'album', 'max-results': 20, 'alt': 'json'})

        while url:
            url = patch_url_params(
                url, fields="link[@rel='next'],"
                            "entry(@gd:etag,link,gphoto:name,title)")
            resp = self.request(self._session, url)
            page = resp.json()['feed']
            for entry in page.get('entry', []):
                yield Album(entry)

            url = link(page, 'next')

    def _list_photos(self, album):
        # change URL for original photos:
        url = patch_url_params(album.feed_url,
                               **{'max-results': 400, 'imgmax': 'd'})

        while url:
            url = patch_url_params(
                url, fields="link[@rel='next'],"
                            "entry(@gd:etag,gphoto:videodownloadurl,content,"
                            "title)")
            resp = self.request(self._session, url)
            page = resp.json()['feed']
            for entry in page.get('entry', []):
                yield Photo(entry, album)

            url = link(page, 'next')

    def download_albums(self):
        for album in self._list_albums():
            self.download_album(album)

    def download_album(self, album):
        for photo in self._list_photos(album):
            self.download_photo(photo)

    def download_photo(self, photo):
        self.q.put(photo)

    def write_file(self, photo, stream):
        for _ in stream:
            pass

    def is_stale(self, photo):
        return True
