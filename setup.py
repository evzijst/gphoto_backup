#!/usr/bin/env python
import os
import sys
from setuptools import setup

requirements = open('requirements.txt').readlines()


def long_description():
    """Get the long description from the README"""
    return open(os.path.join(sys.path[0], 'README.rst')).read()

setup(
    author='Erik van Zijst',
    author_email="erik.van.zijst@gmail.com",
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: End Users/Desktop',
        'Intended Audience :: Other Audience',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Programming Language :: Python :: 2.7',
        'Topic :: System :: Archiving',
        'Topic :: System :: Archiving :: Backup',
        'Topic :: System :: Archiving :: Mirroring'
    ],
    description='Create a full backup of your Google Photos account.',
    keywords='picasaweb backup photos albums google',
    license='MIT',
    long_description=long_description(),
    name='gphoto_backup',
    packages=['gphoto'],
    install_requires=requirements,
    scripts=['bin/gphoto-backup'],
    url='https://bitbucket.org/evzijst/gphoto_backup',
    version=0.17,
)
