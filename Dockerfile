FROM python:2
MAINTAINER Erik van Zijst <erik.van.zijst@gmail.com>

RUN mkdir /opt/service
ADD . /opt/service/
WORKDIR /opt/service
RUN python setup.py install

WORKDIR /backup
CMD ["gphoto-backup", "s3", "/backup"]
